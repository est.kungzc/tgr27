const mqtt = require('mqtt');
const client = mqtt.connect('mqtt://202.139.192.75');
const MongoClient = require('mongodb').MongoClient;

client.on('connect', () => {
  //   client.subscribe('#');
  client.subscribe('/tgr2020/jan08/data/#');
});

const MongoURI = 'mongodb://localhost:32769/tgr27';
MongoClient.connect(
  MongoURI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  function(err, db) {
    if (err) throw err;
    var dbo = db.db('tgr27');
    var collection = dbo.collection('mqtt2');

    client.on('message', function(topic, message) {
      var messageObject = {
        topic: topic,
        message: message.toString()
      };

      collection.insertOne(messageObject, function(err, result) {
        if (err) throw err;
      });
      console.log(messageObject);
    });
  }
);
