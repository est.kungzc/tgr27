var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://202.139.192.75');
var MongoClient = require('mongodb').MongoClient;
var MongoURI = "mongodb://localhost:27017/";
// var MongoURI = 'mongodb://localhost:32769/';
// var MongoURI = "mongodb://202.139.192.209:27017/";

const isJsonString = str => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};

MongoClient.connect(
  MongoURI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  (err, db) => {
    if (err) throw err;
    console.log('DB CONNECTED');
    let dbo = db.db('tgr27');

    client.on('connect', () => {
      console.log('MQTT Connected.');
      client.subscribe('sensor/test');
      client.subscribe('tgr2020/pm25/data/#');
      client.subscribe('tgr2020/track/data/#');
    });

    client.on('message', (topic, message) => {
      const msg = message.toString();
      if (!isJsonString(msg)) {
        console.log('It is not JSON!');
        return;
      }
      const json = JSON.parse(msg);
    //   console.log('eieiei', json);
      dbo.collection('rawData').insertOne(json, function(err, res) {
        if (err) throw err;
        console.log(topic, json);
      });
    });
  }
);
