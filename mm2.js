var mqtt   = require('mqtt');
var client = mqtt.connect('mqtt://202.139.192.75');
var MongoClient = require('mongodb').MongoClient;
 
client.on('connect', function () {
    console.log("MQTT Connected.");
    // client.subscribe('#');
    // client.subscribe('sensor/test');
    client.subscribe('/tgr2020/jan08/data/27');
})


// const MongoURI = 'mongodb://localhost:32769/tgr27';
const MongoURI = 'mongodb://localhost/tgr27';
client.on('message', function (topic, message) {
    //  console.log(topic.toString() + " => " + message.toString())
     MongoClient.connect(MongoURI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, function(err, db) {
        if (err) throw err;
        var dbo = db.db("tgr27");
        var doc = {
            topic: topic,
            message: message.toString()
          };
        dbo.collection("mqtt").insertOne(doc, function(err, res) {
        if (err) throw err;
        console.log("1 document inserted");
        db.close();
        });
    })
})

