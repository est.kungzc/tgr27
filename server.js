const fs = require('fs')
const ini = require('ini')
const mongodb = require('mongodb')
const express = require('express')
const app = express()
const port = 80

var config = ini.parse(fs.readFileSync(__dirname + '/config.ini', 'utf-8'))
var mongo_client = mongodb.MongoClient
var db;
var collection;
mongo_client.connect(config.mongodb.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, function(error, connection) {
    if (error) {
        console.error(error)
    } else {
        db = connection.db("tgr2020");
        collection = db.collection("clean_data");
    }
})

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/api/pm25_data/:sensor_id', function (req, res) {
    const id = req.params.sensor_id;
    collection.find({ sensor_id: id, sensor_type: "pm25"}).sort({ts: -1}).limit(1).next().then(
       function(doc) {
         res.send(doc);
       }
     );
})

app.get('/api/track_data/:id', function (req, res) {
    const id = req.params.id;
    collection.find({ sensor_id: id, sensor_type: "track"}).sort({ts: -1}).limit(1).next().then(
       function(doc) {
         res.send(doc);
       }
     );
})

app.get('/api/pm25_data/:sensor_id/list', async (req, res) => {
  const id = req.params.sensor_id;
  let queryPM25 = {
    sensor_type: 'pm25',
    sensor_id: id
  }

   await collection
   .find(queryPM25)
   .sort({ ts: -1 })
   .limit(100)
   .toArray(function(err, result) {
     if (err) return res.status(404);
     res.send(result);
   });
  })


app.get('/api/track_data/:sensor_id/list', async (req, res) => {
  const id = req.params.sensor_id;
  let queryPM25 = {
    sensor_type: 'track',
    sensor_id: id
  }

   await collection
   .find(queryPM25)
   .sort({ ts: -1 })
   .limit(5)
   .toArray(function(err, result) {
     if (err) return res.status(404);
     res.send(result);
   });
})


app.listen(port, () => console.log(`Example app listening on port ${port}!`))

