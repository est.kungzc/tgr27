var mqtt   = require('mqtt');
var client = mqtt.connect('mqtt://202.139.192.75');
var MongoClient = require('mongodb').MongoClient;
var MongoURI = "mongodb://localhost:32768/";

let mongoIsConnected = false;
let mqttIsConnected = false;

MongoClient.connect(MongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, (err, db) => {
    if (err) {
        throw err;
    }
    mongoIsConnected = true;
    console.log('DB CONNECTED');
    dbo = db.db("tgr27");

    client.on('connect', () => {
        client.subscribe('tgr2020/pm25/data/#');
        client.subscribe('tgr2020/track/data/#');
      })
    
    client.on('message', (topic, message) => {
        if (!!topic){
            mqttIsConnected = true;
        } else {
            mqttIsConnected = false;
        }
        console.log(isConnected());
        
      })
    

});

console.log(isConnected());
function isConnected () {
    const status =  mongoIsConnected && mqttIsConnected;
    return status;
}
test('isConnected MONGODB & MQTT', () => {
    expect(isConnected()).toBe(true);
  });