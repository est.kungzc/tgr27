var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://202.139.192.75');
var MongoClient = require('mongodb').MongoClient;
var MongoURI = "mongodb://127.0.0.1:27017";
var express = require('express')
var app = express()

app.use(express.json())

const isJsonString = (str) => {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

var dbo;

MongoClient.connect(MongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, (err, db) => {
    if (err) throw err;
    console.log('DB CONNECTED');
    dbo = db.db("tgr27");
    client.on('connect', onConnect);
    
    client.on('message', onMessage);

});

const onConnect = () => {
        console.log("MQTT Connected.");
        client.subscribe('tgr2020/pm25/data/#');
        client.subscribe('tgr2020/track/data/#');
};

const onMessage = (topic, message) => {
    const msg = message.toString();
    if(!isJsonString(msg)) {
        console.log('It is not JSON!');
        return;
    }
    console.log(msg);
    const json = JSON.parse(msg);
    dbo.collection("rawData").insertOne(json);

};

app.get('/', function (req, res) {
    res.send('Hello World!')
});

app.post('/', function (req, res) {
    console.log(req.body)
    res.send(req.body)
});

app.listen(3000, function () {
    console.log("Listening on port 3000!");
});
