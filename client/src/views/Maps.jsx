/*!

=========================================================
* Argon Dashboard React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useState, useEffect } from 'react';
import axios from 'axios';
// react plugin used to create google maps
import {
  withGoogleMap,
  withScriptjs,
  GoogleMap,
  Marker,
  InfoWindow
} from 'react-google-maps';
// reactstrap components
import { Card, Container, Row } from 'reactstrap';

// react plugin for datetime
import Moment from 'react-moment';

// core components
import Header from 'components/Headers/Header.jsx';

import feel1 from '../assets/img/icons/feel/feel-1.PNG';
import feel2 from '../assets/img/icons/feel/feel-2.PNG';
import feel3 from '../assets/img/icons/feel/feel-3.PNG';
import feel4 from '../assets/img/icons/feel/feel-4.PNG';
import feel5 from '../assets/img/icons/feel/feel-5.PNG';

async function getLastedData() {
  const g6LastedGet = await axios.get(
    'http://202.139.192.209/api/pm25_data/44/'
  );
  const g9LastedGet = await axios.get(
    'http://202.139.192.209/api/pm25_data/45/'
  );
  const g24LastedGet = await axios.get(
    'http://202.139.192.209/api/pm25_data/46/'
  );
  const g27LastedGet = await axios.get(
    'http://202.139.192.209/api/pm25_data/47/'
  );

  const lastedData = [];

  // console.log(g6LastedGet);

  lastedData.push({
    sensor_id: g6LastedGet.data.sensor_id,
    lat: g6LastedGet.data.location.lat,
    long: g6LastedGet.data.location.long,
    locName: g6LastedGet.data.location.locationName,
    pm25: g6LastedGet.data.pm25
  });
  lastedData.push({
    sensor_id: g9LastedGet.data.sensor_id,
    lat: g9LastedGet.data.location.lat,
    long: g9LastedGet.data.location.long,
    locName: g9LastedGet.data.location.locationName,
    pm25: g9LastedGet.data.pm25
  });
  lastedData.push({
    sensor_id: g24LastedGet.data.sensor_id,
    lat: g24LastedGet.data.location.lat,
    long: g24LastedGet.data.location.long,
    locName: g24LastedGet.data.location.locationName,
    pm25: g24LastedGet.data.pm25
  });
  lastedData.push({
    sensor_id: g27LastedGet.data.sensor_id,
    lat: g27LastedGet.data.location.lat,
    long: g27LastedGet.data.location.long,
    locName: g27LastedGet.data.location.locationName,
    pm25: g27LastedGet.data.pm25
  });
  return lastedData;
}

async function getLastedTrack() {
  const g6track = await axios.get(
    'http://202.139.192.209/api/track_data/6/list'
  );
  const g9track = await axios.get(
    'http://202.139.192.209/api/track_data/9/list'
  );
  const g24track = await axios.get(
    'http://202.139.192.209/api/track_data/24/list'
  );
  const g27track = await axios.get(
    'http://202.139.192.209/api/track_data/27/list'
  );

  const teamArr = [6, 9, 24, 27];

  let trackData = {
    6: g6track.data,
    9: g9track.data,
    24: g24track.data,
    27: g27track.data
  };

  let filterTrackdata = {};

  for (let index = 0; index < 4; index++) {
    if (
      trackData[teamArr[index]] === undefined ||
      trackData[teamArr[index]].length === 0
    ) {
      trackData[teamArr[index]] = [
        {
          _id: '5e17d88f4359185c53a152a8',
          ts: '2020-01-10T08:51:11.579Z',
          sensor_type: 'track',
          sensor_id: '27',
          mac_addr: '80:E1:26:07:E1:50',
          rssi: -99,
          event_code: 8
        }
      ];
      //console.length
    }
  }

  for (let index = 0; index < 4; index++) {
    for (let index2 = 0; index2 < trackData[teamArr[index]].length; index2++) {
      if (trackData[teamArr[index]][index2].mac_addr === '80:E1:26:07:E1:50') {
        filterTrackdata[teamArr[index]] = trackData[teamArr[index]][index2];
        break
      }
      
    }
    if (filterTrackdata[teamArr[index]] === undefined) {
      filterTrackdata[teamArr[index]] = {
        _id: '5e17d88f4359185c53a152a8',
        ts: '2020-01-10T08:51:11.579Z',
        sensor_type: 'track',
        sensor_id: '27',
        mac_addr: '80:E1:26:07:E1:50',
        rssi: -99,
        event_code: 8
      };
    }
  }

  const currentLoc = Object.keys(filterTrackdata).reduce(function(a, b) {
    return filterTrackdata[a].rssi > filterTrackdata[b].rssi ? a : b;
  });

  // console.log(currentLoc)

  const pmLocData = {
    '6': { lat: 19.166391, long: 99.901908, locationName: 'ศาลหลักเหมือง' },
    '9': { lat: 19.177149, long: 99.812571, locationName: 'วัดอนาลโยทิพยาราม' },
    '24': { lat: 19.176477, long: 99.88926, locationName: 'วัดศรีโคมคำ' },
    '27': { lat: 19.021294, long: 99.897926, locationName: 'พระนาคปรก สธ' }
  };

  return {'loc': pmLocData[currentLoc], 'event': filterTrackdata[currentLoc].event_code};
}

function Map() {
  const [selectedData, setSelectedData] = useState(null);
  const [selectedMarker, setSelectedMarker] = useState(null);
  const [pin, setPin] = useState(null);
  const [event, setEvent] = useState(null);

  useEffect(() => {
    getLastedData().then(res => {
      setSelectedData(res);
    });

    getLastedTrack().then(res => {
      setPin(res.loc);
      setEvent(res.event);
    });
  }, []);

  useEffect(() => {
    const listener = e => {
      if (e.key === 'Escape') {
        setSelectedMarker(null);
      }
    };
    window.addEventListener('keydown', listener);

    return () => {
      window.removeEventListener('keydown', listener);
    };
  }, []);
  console.log('render >>>', selectedData);

  return (
    <GoogleMap
      defaultZoom={10}
      defaultCenter={{ lat: 19.027286, lng: 99.900246 }}
      defaultOptions={{
        scrollwheel: false
      }}
    >
      {pin != null ? (
        <Marker
          position={{
            lat: pin.lat,
            lng: pin.long
          }}
          onClick={() => {
            setSelectedMarker(pin);
          }}
          icon={{
            url: `http://maps.google.com/mapfiles/ms/icons/purple-dot.png`
          }}
        />
      ) : (
        ''
      )}
      {selectedData != null
        ? selectedData.map((data, index) => (
            <Marker
              key={index}
              position={{
                lat: data.lat,
                lng: data.long
              }}
              onClick={() => {
                setSelectedMarker(data);
              }}
              icon={{
                url: `http://maps.google.com/mapfiles/ms/icons/${
                  data.avgPm25 <= 50
                    ? 'ltblue-dot'
                    : data.avgPm25 <= 100
                    ? 'green-dot'
                    : data.avgPm25 <= 200
                    ? 'yellow-dot'
                    : data.avgPm25 <= 300
                    ? 'orange-dot'
                    : 'red-dot'
                }.png`
              }}
            />
          ))
        : ''}
      {selectedMarker && (
        <InfoWindow
          onCloseClick={() => {
            setSelectedMarker(null);
          }}
          position={{
            lat: selectedMarker.lat,
            lng: selectedMarker.long
          }}
        >
          <div>
            <div className="text-center">
              <img
                src={`${
                  selectedMarker.avgPm25 <= 50
                    ? feel1
                    : selectedMarker.avgPm25 <= 100
                    ? feel2
                    : selectedMarker.avgPm25 <= 200
                    ? feel3
                    : selectedMarker.avgPm25 <= 300
                    ? feel4
                    : feel5
                }`}
                width="110"
                height="82"
              />
            </div>
            <div className="text-center">
              <h4>PM2.5 : {selectedMarker.avgPm25}</h4>
              <h5>{`${
                selectedMarker.avgPm25 <= 50
                  ? 'Good'
                  : selectedMarker.avgPm25 <= 100
                  ? 'Moderate'
                  : selectedMarker.avgPm25 <= 200
                  ? 'Unhealthy'
                  : 'Hazardous'
              }`}</h5>
            </div>
            <div className="text-center">
              <p>
                <strong>Location name:</strong> {selectedMarker.locName}
              </p>
              <p>
                <strong>Lat:</strong> {selectedMarker.lat}
              </p>
              <p>
                <strong>Long:</strong> {selectedMarker.long}
              </p>
            </div>
            <div className="text-center">
              Lastet Time:{' '}
              <Moment format="D MMM YYYY, H:m" withTitle>
                {selectedMarker.lastedTimestamp}
              </Moment>
            </div>
          </div>
        </InfoWindow>
      )}
    </GoogleMap>
  );
}

const MapWrapper = withScriptjs(withGoogleMap(Map));

class Maps extends React.Component {
  state = {
    event: 0
  }
  componentDidMount() {
    axios.get(
      'http://202.139.192.209/api/track_data/27/'
    ).then(res => {
      this.setState = {
        event: res.data.event_code,
      }
      console.log(res.data.event_code)
    });
  }
  render() {
    return (
      <>
        {/* <Header /> */}
        {/* Page content */}
        <Container className="mt--7" fluid>
          <Row>
            <div className="col">
              <Card className="shadow border-0">
              <h1 className="text-center">
        {this.state.event == 8 ? 'หมดสติ' : this.state.event == 128 ? 'ล้มแล้วหมดสติ' : this.state.event == 255 ? 'ล้มสะดุด' : '-'}
        </h1>
                <MapWrapper
                  googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3HB2XOBjkvNkEZ7Yb09Dfu48lSxPePno"
                  loadingElement={<div style={{ height: `100%` }} />}
                  containerElement={
                    <div
                      style={{ height: `600px` }}
                      className="map-canvas"
                      id="map-canvas"
                    />
                  }
                  mapElement={
                    <div style={{ height: `100%`, borderRadius: 'inherit' }} />
                  }
                />
              </Card>
            </div>
          </Row>
        </Container>
      </>
    );
  }
}

export default Maps;
